var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')({ camelize: true }),
    lr = require('tiny-lr'),
    server = lr()
var fs = require('fs')

gulp.task('deploy', ['default'], function() {
  rsync({
    ssh: true,
    src: './',
    dest: 'deploy@104.236.59.156:/home/deploy/deploy',
    port: 4491,
    recursive: true,
    syncDest: true,
    include: ['assets/js/lib'],
    exclude: ['lib', 'assets/css/build', 'assets/css/src', 'node_modules',
              '.git', '.gitignore', '*.swp', '.sass-cache', 'deploy',
              'assets/js/build', 'assets/js/src', 'assets/manifest.json',
              'assets/img/instagram-0.jpg', 'assets/img/instagram-1.jpg',
              'assets/img/instagram-2.jpg'],
    args: ['--verbose']
  }, function(error, stdout, stderr, cmd) {
      console.log(stdout);
  });
});
